/***************************************************************************
*File			: 02Summation.cpp
*Description	: Program to find the sum of all the natural numbers from 1 to n.
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 26 December 2022
***************************************************************************/

#include <iostream>
#include <iomanip>
using namespace std;

/***************************************************************************
*Function		: 	main
*Input parameters	:	no parameters
*RETURNS		:	0 on success
***************************************************************************/

int main() {
    int iNum, iSum = 0;

    cout << "Enter the value of n : " ;
    cin >> iNum;

    for(int i=1;i<=iNum;i++){
        iSum += i;
    }
    cout << "Sum of 1 to " << iNum << " is : " << iSum << endl;
    cout << "Sum of 1 to " << iNum << " using formula is : " << iNum*(iNum+1)/2 << endl;
    return 0;
}
