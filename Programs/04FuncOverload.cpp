/***************************************************************************
*File			: 04FuncOverload.cpp
*Description	: Program to demonstrate function overloading
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 26 December 2022
***************************************************************************/

#include <iostream>
#include <iomanip>
using namespace std;

//Function Prototypes
void add(int, int);
void add(double,  double);

/***************************************************************************
*Function		: 	main
*Input parameters	:	no parameters
*RETURNS		:	0 on success
***************************************************************************/

int main() {
    int iNum1, iNum2;
    double dVal1, dVal2;


    cout << "Enter integer values for i1 and i2 : " ;
    cin >> iNum1 >> iNum2;

    cout << "Enter double values for d1 and d2 : " ;
    cin >> dVal1 >> dVal2;

    add(iNum1, iNum2);
    add(dVal1, dVal2);

    return 0;
}

void add(int p, int q)
{
    cout << "Performing Integer Addition" << endl;
    cout << "Sum of " << p << " and " << q << " is " << p+q << endl;
}

void add(double p, double q)
{
    cout << "Performing Double Addition" << endl;
    cout << "Sum of " << p << " and " << q << " is " << p+q << endl;
}
