/***************************************************************************
*File			: 01SortElem.cpp
*Description	: Program to sort the elements in ascending and descending order.
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 26 December 2022
***************************************************************************/

#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

/***************************************************************************
*Function		: 	main
*Input parameters	:	no parameters
*RETURNS		:	0 on success
***************************************************************************/
int main() {
    vector<int> values;
    int iNum, iElem;

    cout << "Enter the number of elements : " ;
    cin >> iNum;

    cout << "Enter " << iNum << " values :"; 
    for(int i=0;i<iNum;i++){
        cin >> iElem;
        values.push_back(iElem);
    }

    for(int i=0;i<iNum;i++){
        cout << values[i] << "  ";
    }
    cout << endl;


    // sort(values.begin(), values.end());
    //sorting in ascending order
    cout << "Sorting in descending order" << endl;
    for(int i=0;i<iNum;i++){
        for(int j=0;j<iNum;j++){
            if(values[i] < values[j]){
                int temp = values[i];
                values[i] = values[j];
                values[j] = temp;
            }
        }
    }    

    for(int i=0;i<iNum;i++){
        cout << values[i] << "  ";
    }
    cout << endl;

    // sort(values.begin(), values.end(), greater<int>());
    //sorting in descending order
    cout << "Sorting in descending order" << endl;
    for(int i=0;i<iNum;i++){
        for(int j=0;j<iNum;j++){
            if(values[i] > values[j]){
                int temp = values[i];
                values[i] = values[j];
                values[j] = temp;
            }
        }
    }    

    for(int i=0;i<iNum;i++){
        cout << values[i] << "  ";
    }
    cout << endl;
    return 0;
}
